extends Node



# Declare signals here
#--------------------------------------------#
signal visibility_button

# Declare member variables here. 
#--------------------------------------------#

# Name for my player.
var player_name = "The Warrior"

# Network peer to use as networking connection
var peer = null

# Dictionary with all connected players
var players_dict = {}

# Default game port. Can be any number between 1024 and 49151.
const DEFAULT_PORT = 10567

# Max number of players.
const MAX_PEERS = 12

# Declare functions here:
#--------------------------------------------#

func _change_visibility_button():
	print("button pressed")
	emit_signal( "visibility_button")


func _player_connected(new_player_name):
	print("player connected")

func host_game(new_player_name):
	player_name = new_player_name
	peer = NetworkedMultiplayerENet.new()
	peer.create_server(DEFAULT_PORT, MAX_PEERS)
	get_tree().set_network_peer(peer)
	print("hosted")
	
func join_game(ip, new_player_name):
	player_name = new_player_name
	peer = NetworkedMultiplayerENet.new()
	peer.create_client(ip, DEFAULT_PORT)
	get_tree().set_network_peer(peer)
	print("joined")
	
	
# Called when the node enters the scene tree for the first time.
func _ready():
	get_tree().connect("network_peer_connected", self, "_player_connected")
	get_tree().connect("network_peer_disconnected", self,"_player_disconnected")
	get_tree().connect("connected_to_server", self, "_connected_ok")
	get_tree().connect("connection_failed", self, "_connected_fail")
	get_tree().connect("server_disconnected", self, "_server_disconnected")
	
	
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
