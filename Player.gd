extends Node2D

export var speed = 400

var screen_size

# The player's movement vector.
var velocity = Vector2()  

var animationInfo 

# The position Vector of the player model. 
# -> Redefined to be able to be send over network
var positionVector = Vector2(position.x, position.y)

var last_positionVector = Vector2()

func _ready():
	screen_size = get_viewport_rect().size
	
	# Defines camera as current if:
	# A: There is no network connection defined 
	# B: The current client ID
	if not get_tree().get_network_peer() or get_tree().get_network_unique_id() == int(name):
		$Camera2D.current = true


# This function runs at a fixed rate (delta). The timedelta delta is constant. 
# Since this function runs at a constant tick rate, it will be used to sync the multiplayer.
func _physics_process(delta):
	last_positionVector = position
#	print("is_network_master: ", is_network_master())
	if not get_tree().get_network_peer() or is_network_master():
		velocity = Vector2()  # The player's movement vector.
		if Input.is_action_pressed("ui_right"):
			velocity.x += 1
		if Input.is_action_pressed("ui_left"):
			velocity.x -= 1
		if Input.is_action_pressed("ui_down"):
			velocity.y += 1
		if Input.is_action_pressed("ui_up"):
			velocity.y -= 1
		
		if velocity.length() > 0:
			velocity = velocity.normalized() * speed
		
		positionVector += velocity * delta
		positionVector.x = clamp(positionVector.x, 0, screen_size.x)
		positionVector.y = clamp(positionVector.y, 0, screen_size.y)
		
		# The position will only be updated when a change is detected (reduces network load)
		if positionVector != last_positionVector:
			position = positionVector
			playAnimation()
			$AnimatedSprite.flip_v = velocity.x < 0
#			print("position: ", position)
			animationInfo = [$AnimatedSprite.animation, $AnimatedSprite.frame, $AnimatedSprite.flip_v]
			rpc("syncPlayerPos", positionVector, animationInfo)


# remote function for player position synchronisation. 
# warning-ignore:shadowed_variable
# warning-ignore:shadowed_variable
remote func syncPlayerPos(positionVector, animationInfo):
	position = positionVector
	playAnimation()
#	$AnimatedSprite.animation	= animationInfo[0]
#	$AnimatedSprite.frame		= animationInfo[1]
	$AnimatedSprite.flip_v		= animationInfo[2]

# Function to play the animation once. Once the end of the animation is detected 
# via the signal through _on_AnimatedSprite_animation_finished.
# The mentioned function will disable the playback.
func playAnimation():
	$AnimatedSprite.animation = "walk"
	$AnimatedSprite.play()

# On signal animation finished, the playback will be stopped.
func _on_AnimatedSprite_animation_finished():
	$AnimatedSprite.stop()




func _on_KillCollision_area_entered(area):
	print("Entered_Area")
	var rect = get_tree().get_root().get_node("/root/MainScene/Kill_Tester")
	var rect_path = rect.get_path()
	print(rect_path)
	if rect:
		colourRectangle(rect_path)
		rpc("colourRectangle", rect_path)


remote func colourRectangle(rect_path):
	get_tree().get_root().get_node(rect_path).set_modulate(Color(0.643137,0.141176,0.141176,1))
	
	
func _on_KillCollision_area_exited(area):
	print("Left_Area")
	var rect = get_tree().get_root().get_node("/root/MainScene/Kill_Tester")
	var rect_path = rect.get_path()
	print(rect_path)
	if rect:
		decolourRectangle(rect_path)
		rpc("decolourRectangle", rect_path)

		
remote func decolourRectangle(rect_path):
	get_tree().get_root().get_node(rect_path).set_modulate(Color(255,255,255,1))
	
