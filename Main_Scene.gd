extends Node2D







# Called when the node enters the scene tree for the first time.
func _ready():
	# Testing Bay
	#--------------------------------------------#
	print(get_tree().get_root())
	print(get_node("../Node2D/Node2D"))
	print(get_node("../Node2D"))

# This function runs every frame. It is DEPENDANT ON THE CLIENTS FRAMERATE. 
# The elapsed time between frames is delta.
func _process(delta):
	pass

# This function runs at a fixed rate (delta). The timedelta delta is constant. 
# Since this function runs at a constant tick rate, it will be used to sync the multiplayer.
func _physics_process(delta):
	pass


func syncPlayerPosition():
#	rpc("syncPlayerPositionPuppet", playerid, position)
#
#puppet func syncPlayerPositionPuppet(playerid, position):
	pass
	

func _on_Timer_timeout():
	# Note: the `$` operator is a shorthand for `get_node()`,
	# so `$Sprite` is equivalent to `get_node("Sprite")`.
	$Sprite.visible = !$Sprite.visible
#	print("change timer visibility")

func _on_Button_Change_Vis_pressed():
	rpc("change_visibility")
	
	
sync func change_visibility():
	$Sprite.visible = !$Sprite.visible
	print("Button pressed")


func _on_Button_Host_pressed():
	gamestate.host_game("testname")


func _on_Button_Join_pressed():
	gamestate.join_game("127.0.0.1","testname")


func _on_Button_Add_Player_pressed():
	var network_id = get_tree().get_network_unique_id()
	add_player(network_id)
	rpc("add_player", network_id)

remote func add_player(network_id):
	print("network_id of new playermodel: ",network_id)
	
	gamestate.players_dict[network_id] = ["Dummydata"]
	var Player = load("res://Player.tscn").instance()
	Player.set_name(str(network_id))
	get_tree().get_root().add_child(Player)
	

	Player.set_network_master(network_id)
	
	print("Player_name: ", Player.name)



func _on_Button_Slave_pressed():
	print("is_network_master: ", is_network_master())
	rpc("disableSpriteSlave")


func _on_Button_Master_pressed():
	print("is_network_master: ", is_network_master())
	rpc("disableSpriteMaster")


master func disableSpriteMaster():
	$Sprite.visible = ! $Sprite.visible
	
	
puppet func disableSpriteSlave():
	$Sprite.visible = ! $Sprite.visible
	
